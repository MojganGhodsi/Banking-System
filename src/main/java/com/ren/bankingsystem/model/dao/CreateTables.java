package com.ren.bankingsystem.model.dao;

import com.ren.bankingsystem.model.dao.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by user on 2017-10-07.
 */

public class CreateTables {
    private final static CreateTables create = new CreateTables();

    private ConnectionPool conn ;
  //  private  String dbURL = "jdbc:derby://localhost:1527/ContactDao;create=true";
    private  String tableName = "customer";
    private  String tableName2="deposit";
    private Connection connection;
    private Statement stmt;
    private ArrayList tablelist = new ArrayList();

    private CreateTables() {
    }
    public static CreateTables getInstance(){
        return create;
    }

    public void checkTableExistance() {


        try {
           // connection = conn.barrow();
            Statement stmt = connection.createStatement();
            ResultSet set = stmt.executeQuery("SELECT TABLENAME FROM SYS.SYSTABLES");
            while ((set.next())) {
                tablelist.add(set.getString("TABLENAME"));
            }
            if (!tablelist.contains(tableName) && !tablelist.contains(tableName2)) {
                createTables(tableName, tableName2);
            }


        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }



    public void createTables(String tableName, String tableName2) {

        try {
           // connection=conn.barrow();
            stmt = connection.createStatement();
            stmt.execute("CREATE TABLE " + tableName2 + "(id int NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) ,name varchar(255),family varchar (255),customerNumber varchar (255),email varchar (255),)");


            stmt.execute("CREATE TABLE " + tableName + "(uId int, depositNumber varchar(255),balance varchar (255))");
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(" error was happen in createTables method");
        }
    }

}
