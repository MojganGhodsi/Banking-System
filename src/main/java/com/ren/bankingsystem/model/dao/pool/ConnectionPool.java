package com.ren.bankingsystem.model.dao.pool;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by My PC on 11/3/2017.
 */
public final class ConnectionPool {
        private final static int INITIAL_NUMBER_OF_OBJECT = 3;
        private final static int MAX_NUMBER_OF_OBJECT = 10;
        private static ConnectionPool instance=new ConnectionPool();
        private ArrayList<Connection> freeList = new ArrayList<Connection>(MAX_NUMBER_OF_OBJECT);
        private ArrayList<Connection> usedList = new ArrayList<Connection>(MAX_NUMBER_OF_OBJECT);
        private  String dbURL;

    public ConnectionPool() {

    }
    public void init(){
        dbURL="jdbc:derby:Bank;create=true";
        addToPool(INITIAL_NUMBER_OF_OBJECT);
    }

    public static ConnectionPool getInstancen(){
        return instance;
    }

    private void addToPool(int initialNumberOfObject) {
        for (int i=0; i < initialNumberOfObject; i++)
            try {
                freeList.add((DriverManager.getConnection(dbURL)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    public Connection barrow(){
        Connection c = null;

        if (usedList.size() <= MAX_NUMBER_OF_OBJECT
                && freeList.size() == 0) addToPool(1);

        if (!freeList.isEmpty()){
            c = freeList.get(0);
            usedList.add(c);
            freeList.remove(c);
        } else {
            System.out.println("No Connection");
            return null;
        }

        return c;
    }
    public void release (Connection c) {

        if ( c != null && usedList.contains(c) ){
            usedList.remove(c);
            freeList.add(c);
        }

    }

    }


