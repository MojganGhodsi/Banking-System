package com.ren.bankingsystem.model.entity;

/**
 * Created by My PC on 11/4/2017.
 */
public class CustomerEnity {
        private String name;
        private String familyName;
        private String customerNumber;
        private String email;


        public CustomerEnity(){
        }

        public CustomerEnity(String name, String familyname,String customerNumber ,String email){
            this.name=name;
            this.familyName=familyname;
            this.customerNumber=customerNumber;
            this.email=email;
        }

        public void setName(String name){
            this.name=name;
        }
        public void setFamilyName(String familyName){
            this.familyName=familyName;
        }
        public void setCustomerNumber(String customerNumber)
        {
            this.customerNumber=customerNumber;
        }
        public void setEmail(String email){
            this.email=email;
        }

        public String getName(){
            return name;
        }
        public String getFamilyName(){
            return familyName;
        }
        public String getCustomerNumber(){
            return customerNumber;
        }
        public String getEmail(){
            return email;
        }

}
