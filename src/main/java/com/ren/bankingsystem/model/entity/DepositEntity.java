package com.ren.bankingsystem.model.entity;

/**
 * Created by My PC on 11/4/2017.
 */
public class DepositEntity {
    private String depositNumber;
    private long balance;


    public DepositEntity(){
    }

    public DepositEntity(String depositNumber, long balance){
        this.depositNumber=depositNumber;
        this.balance=balance;
    }

    public void setDepositNumber(String depositNumber){
        this.depositNumber=depositNumber;
    }
    public void setBalancee(long balance) {this.balance = balance;}
    public String getDepositNumber(){
        return depositNumber;
    }
    public long getbalance(){
        return balance;
    }
}
