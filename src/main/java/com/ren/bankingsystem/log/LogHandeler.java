package com.ren.bankingsystem.log;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by My PC on 11/3/2017.
 */
public class LogHandeler {

        private static LogHandeler instance = null;
        private static final Logger logger = Logger.getLogger("BankingSystemLog");
        private FileHandler fileHandler;

        public static LogHandeler getInstance() {
            if (instance == null) {
                instance = new LogHandeler();
            }
            return instance;
        }

        private LogHandeler() {
            try {
                fileHandler = new FileHandler("Banking System.txt", true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            logger.addHandler(fileHandler);
            logger.setLevel(Level.WARNING);
        }

        public void setWarningLog(String msg) {
            logger.warning(msg);
        }
        public void setInfoLog(String msg) {
            logger.info(msg);
        }
    }




