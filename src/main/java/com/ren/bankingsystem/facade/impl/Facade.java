package com.ren.bankingsystem.facade.impl;

import com.ren.bankingsystem.control.impl.Service;
import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.dto.DepositDTO;
import com.ren.bankingsystem.facade.Ifacade;
import com.ren.bankingsystem.model.entity.CustomerEnity;
import com.ren.bankingsystem.model.entity.DepositEntity;

/**
 * Created by My PC on 11/3/2017.
 */
public class Facade implements Ifacade<CustomerDTO,DepositDTO> {
    private CustomerEnity customerEnity;
    private Service service=new Service();

    public  Facade(){

    }



    @Override
    public void mapCustomer(CustomerDTO customerDTO) {
        CustomerEnity ce =new CustomerEnity(customerDTO.getName(), customerDTO.getFamilyName(),customerDTO.getCustomerNumber(),customerDTO.getEmail());

    }

    @Override
    public void mapDposit(DepositDTO depositDTO) {
        DepositEntity de =new DepositEntity(depositDTO.getDepositNumber(),depositDTO.getbalance());

    }
}
