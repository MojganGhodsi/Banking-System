package com.ren.bankingsystem.facade;

import com.ren.bankingsystem.dto.DepositDTO;

/**
 * Created by My PC on 11/3/2017.
 */
public interface Ifacade<CustomerDTO , DepositDTO> {
    public void mapCustomer(CustomerDTO CustomerDto);
    public void mapDposit(DepositDTO depositDTO);
}