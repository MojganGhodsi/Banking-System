//package com.ren.bankingsystem.reports;
//
///**
// * Created by My PC on 11/4/2017.
// */
//
//import com.ren.bankingsystem.log.LogHandeler;
//import com.ren.bankingsystem.properties.PropertyHandeler;
//import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
//import net.sf.dynamicreports.report.builder.datatype.DataTypes;
//import net.sf.dynamicreports.report.builder.style.StyleBuilder;
//import net.sf.dynamicreports.report.constant.HorizontalAlignment;
//import net.sf.dynamicreports.report.datasource.DRDataSource;
//import net.sf.dynamicreports.report.exception.DRException;
//
//import java.awt.*;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.util.ArrayList;
//
//import static net.sf.dynamicreports.report.builder.DynamicReports.col;
//import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
//import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
//import net.sf.dynamicreports.report.builder.datatype.DataTypes;
//import net.sf.dynamicreports.report.builder.style.StyleBuilder;
//import net.sf.dynamicreports.report.constant.HorizontalAlignment;
//import net.sf.dynamicreports.report.datasource.DRDataSource;
//import net.sf.dynamicreports.report.exception.DRException;
//
//
//import java.awt.*;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.util.ArrayList;
//
//import static net.sf.dynamicreports.report.builder.DynamicReports.col;
//import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
//
//public class DepositsReport extends Report{
//
//    private PropertyHandeler property ;
//    private DRDataSource dataSource = new DRDataSource("ID", "NUMBER" , "TYPE");
//
//
//    public void setColumns() {
//
//        StyleBuilder boldStyle = stl.style().bold();
//        StyleBuilder boldCenteredStyle = stl.style(boldStyle)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//        StyleBuilder columnTitleStyle  = stl.style(boldCenteredStyle)
//                .setBorder(stl.pen1Point())
//                .setBackgroundColor(Color.LIGHT_GRAY);
//
//
//        TextColumnBuilder<Integer> idColumn  = col.column("ID", "ID",  DataTypes.integerType())
//                .setFixedColumns(5)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        TextColumnBuilder<String>  numberColumn = col.column("Number" , "NUMBER" , DataTypes.stringType())
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        TextColumnBuilder<String >  typeColumn = col.column("Type" , "TYPE" , DataTypes.stringType())
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//
//        this.getReport().setColumnTitleStyle(columnTitleStyle)
//
//
//                .columns(
//
//                        idColumn ,
//                        numberColumn ,
//                        typeColumn)
//
//                .setColumnTitleStyle(columnTitleStyle);
//
//    }
//
//    @Override
//    public void setData(ArrayList<ContactEntity> contacts) {
//
//        for (ContactEntity c : contacts){
//
//            setContactNumbers(c);
//
//        }
//
//        this.getReport().setDataSource(dataSource);
//
//    }
//
//    private void setContactNumbers(ContactEntity c) {
//
//        for (NumberEntity n : c.getNumbers()){
//
//            dataSource.add(c.getId() , n.getNumber() , n.getType().toString());
//        }
//    }
//
//
//    @Override
//    public void save() //throws CostumeException
//    {
//
////        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:/annotation/config.xml");
//        property =property.getInstance();
//
//        //export the report as a pdf file
//        try {
//
//            property.loadProperties();
//            this.getReport().show();
//            this.getReport().toPdf(new FileOutputStream(property.getProperties("NumbersPdfStoragePath")));
//            this.getReport().toXlsx(new FileOutputStream(property.getProperties("NumbersExcelStoragePath")));
//
//        } catch (DRException | FileNotFoundException e) {
//            LogHandeler.getInstance().setWarningLog(e.toString());
//           // throw new CostumeException("Problems found!" , e);
//        }
//
//    }
//}
