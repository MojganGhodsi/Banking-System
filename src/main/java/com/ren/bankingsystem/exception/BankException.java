package com.ren.bankingsystem.exception;

/**
 * Created by My PC on 11/4/2017.
 */
public class BankException extends Exception{

        public BankException(String message) {
            super(message);
        }

}
