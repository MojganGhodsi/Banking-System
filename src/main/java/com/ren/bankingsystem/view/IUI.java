package com.ren.bankingsystem.view;

/**
 * Created by My PC on 11/1/2017.
 */
public interface IUI {
   void displayMainMenu();
   void displayCustomerMenu();
   void displayDepositMenu();
   void displayReportMenu();
   void displayBackupMenu();
   void exitProgram();

}
