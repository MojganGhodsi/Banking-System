package com.ren.bankingsystem.view;

import com.ren.bankingsystem.dto.CustomerDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by My PC on 11/7/2017.
 */
@WebServlet(value="/insert")
public class InsertCustomerServlet extends HttpServlet {



//    c.setFamilyName("jb");



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         CustomerDTO c = new CustomerDTO(req.getParameter("firstname"),req.getParameter("lastname"),req.getParameter("customernumber"),req.getParameter("email"));
//       req.getParameter("firstname");
//       req.getParameter("lastname");

        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();

        out.print(c.getName() + '\n' + c.getFamilyName() + '\n' + c.getCustomerNumber() + '\n' + c.getEmail());
//
        out.close();

    }
}