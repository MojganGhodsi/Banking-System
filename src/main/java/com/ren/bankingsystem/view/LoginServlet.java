package com.ren.bankingsystem.view;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by My PC on 11/12/2017.
 */
//@WebServlet(value="*.html")
@WebServlet(value="/login")
public class LoginServlet implements Servlet {

    public void init(ServletConfig servletConfig) throws ServletException {

    }

    public ServletConfig getServletConfig() {
        return null;
    }

    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {


        servletResponse.setContentType("text/html;charset=UTF-8");
        PrintWriter out = servletResponse.getWriter();

        out.println("<html> Hello");
        out.println("</html>");
        out.close();
    }

    public String getServletInfo() {
        return null;
    }

    public void destroy() {

    }
}
