package com.ren.bankingsystem.view.impl;

import com.ren.bankingsystem.control.validationcheck.ValidationCheck;
import com.ren.bankingsystem.control.validationcheck.Value;
import com.ren.bankingsystem.dto.CustomerDTO;
import com.ren.bankingsystem.facade.impl.Facade;
import com.ren.bankingsystem.log.LogHandeler;
import com.ren.bankingsystem.view.IUI;
import java.util.Scanner;

/**
 * Created by My PC on 11/1/2017.
 */
public class UI implements IUI {
    private static UI instance = null;
    Facade facade=new Facade();
    private LogHandeler logHandeler = LogHandeler.getInstance();
    private ValidationCheck vc = ValidationCheck.getInstance();
    public static UI getInstance() {
        if (instance == null) {
            instance = new UI();
        }
        return instance;
    }



    @Override
    public void displayMainMenu() {
        Scanner scanner = new Scanner(System.in);
        while(true){

            System.out.println("Enter number of your request:"+'\n'+
                    "1)Customer" + '\n' +
                    "2)Deposit" + '\n' +
                    "3)Report"+ '\n' +
                    "4)backup"+ '\n' +
                    "5)Exit");
            int w = scanner.nextInt();
            switch (w){

                case 1 :
                    logHandeler.setInfoLog("start of customer's menue");
                    displayCustomerMenu();
                    logHandeler.setInfoLog("End of customer's menue");
                    break;

                case 2 :
                    logHandeler.setInfoLog("start of deposit menue");
                    displayDepositMenu();
                    logHandeler.setInfoLog("End of deposit menue");
                    break;
                case 3 :

                    logHandeler.setInfoLog("start of report menue");
                    displayReportMenu();
                    logHandeler.setInfoLog("End of report menue");
                    break;

                case 4 :

                    logHandeler.setInfoLog("start of backup menue");
                    displayBackupMenu();
                    logHandeler.setInfoLog("End of backup menue");
                    break;

                case 5 :
                    System.exit(0);
                    break;

                default :
                    System.out.println("Invalid request"+'\n'+"please try again!");
                    break;
            }
        }
}

    @Override
    public void displayCustomerMenu() {
        CustomerDTO cDTO= new CustomerDTO();
        Scanner scanner = new Scanner(System. in);
        while(true){
            System.out.println("Enter number of your request:"+'\n'+
                    "1)insert" + '\n' +
                    "2)update" + '\n' +
                    "3)delete"+ '\n' +
                    "4)back");
            switch (scanner.nextInt()){

                case 1 :
                    System.out.println("name:");
                    String sn= scanner.next();
                    vc.checkValidation(sn, Value.NAME);
                    cDTO.setName(sn);

                    System.out.println("family:");
                    String sf= scanner.next();
                    vc.checkValidation(sf,Value.NAME);
                    cDTO.setFamilyName(sf);

                    System.out.println("your Customer number:");
                    String s=scanner.next();
                    vc.checkValidation(s,Value.CUSTOMERNUMBER);
                    cDTO.setCustomerNumber(s);

                    facade.mapCustomer(cDTO);
                    logHandeler.setInfoLog("customer is inserted");
                    break;

                case 2 :


                    logHandeler.setInfoLog("End of deposit menue");
                    break;
                case 3 :


                    logHandeler.setInfoLog("End of report menue");
                    break;

                case 4 :

                    logHandeler.setInfoLog("End of backup menue");
                    break;

                case 5 :
                    System.exit(0);
                    break;

                default :
                    displayMainMenu();
                    break;
            }
        }

    }

    @Override
    public void displayDepositMenu() {
        Scanner scanner = new Scanner(System. in);
        while(true){
            System.out.println("Enter number of your request:"+'\n'+
                    "1)insert" + '\n' +
                    "2)update" + '\n' +
                    "3)delete"+ '\n' +
                    "3)deposit"+ '\n' +
                    "3)withdraw"+ '\n' +
                    "3)transfer"+ '\n' +
                    "4)back");
            switch (scanner.nextInt()){
                    case 1 :
                        logHandeler.setInfoLog("start of customer's menue");

                        logHandeler.setInfoLog("End of customer's menue");
                        break;

                    case 2 :
                        logHandeler.setInfoLog("start of deposit menue");

                        logHandeler.setInfoLog("End of deposit menue");
                        break;
                    case 3 :

                        logHandeler.setInfoLog("start of report menue");

                        logHandeler.setInfoLog("End of report menue");
                        break;

                    case 4 :

                        logHandeler.setInfoLog("start of backup menue");

                        logHandeler.setInfoLog("End of backup menue");
                        break;

                    case 5 :
                        displayMainMenu();
                        break;

                    default :
                        System.out.println("Invalid request"+'\n'+"please try again!");
                        break;

            }
        }
    }

    @Override
    public void displayReportMenu() {
        Scanner scanner = new Scanner(System. in);
        while(true){
            System.out.println("Enter number of your request:"+'\n'+
                    "1)deposit" + '\n' +
                    "2)costumer" + '\n' +
                    "3)DepositForACustomer"+ '\n' +
                    "4)back");
            switch (scanner.nextInt()){
                    case 1 :
                        logHandeler.setInfoLog("start of customer's menue");

                        logHandeler.setInfoLog("End of customer's menue");
                        break;

                    case 2 :
                        logHandeler.setInfoLog("start of deposit menue");

                        logHandeler.setInfoLog("End of deposit menue");
                        break;
                    case 3 :

                        logHandeler.setInfoLog("start of report menue");

                        logHandeler.setInfoLog("End of report menue");
                        break;

                    case 4 :

                        logHandeler.setInfoLog("start of backup menue");

                        logHandeler.setInfoLog("End of backup menue");
                        break;

                    case 5 :
                        displayMainMenu();
                        break;

                    default :
                        System.out.println("Invalid request"+'\n'+"please try again!");
                        break;
                }

            }
        }


    @Override
    public void displayBackupMenu() {
        Scanner scanner = new Scanner(System. in);
        while(true){
            System.out.println("Enter number of your request:"+'\n'+
                    "1)import" + '\n' +
                    "2)export" + '\n' +
                    "3)back");
            switch (scanner.nextInt()){
                case 1 :
                    logHandeler.setInfoLog("start of customer's menue");

                    logHandeler.setInfoLog("End of customer's menue");
                    break;

                case 2 :
                    logHandeler.setInfoLog("start of deposit menue");

                    logHandeler.setInfoLog("End of deposit menue");
                    break;
                case 3 :

                    logHandeler.setInfoLog("start of report menue");

                    logHandeler.setInfoLog("End of report menue");
                    break;

                case 4 :

                    logHandeler.setInfoLog("start of backup menue");

                    logHandeler.setInfoLog("End of backup menue");
                    break;

                case 5 :
                    displayMainMenu();
                    break;

                default :
                    System.out.println("Invalid request"+'\n'+"please try again!");
                    break;

            }
        }
    }

    @Override
    public void exitProgram() {
        logHandeler.setInfoLog("exiting...");
        System.exit(0);

    }


}

