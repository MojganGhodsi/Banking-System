package com.ren.bankingsystem.view.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by My PC on 11/12/2017.
 */
@WebFilter(urlPatterns = {"/login"})
public class LoginFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest)servletRequest).getSession(true);
        Object user = session.getAttribute("user");
        if(user != null||("zhara".equals(servletRequest.getParameter("user"))&&"12345678".equals(servletRequest.getParameter("pass")))){
            session.setAttribute("user", servletRequest.getParameter("user"));
            filterChain.doFilter(servletRequest,servletResponse);
        }else{
            PrintWriter writer = servletResponse.getWriter();
            writer.print("<form><div class=\"container\">\n" +
                    "    <label><b>Username</b></label>\n" +
                    "    <input type=\"text\" placeholder=\"Enter Username\" name=\"user\" required>\n" +
                    "\n" +
                    "    <label><b>Password</b></label>\n" +
                    "    <input type=\"password\" placeholder=\"Enter Password\" name=\"pass\" required>\n" +
                    "\n" +
                    "    <button type=\"submit\">Login</button>\n" +
                    "    <input type=\"checkbox\" checked=\"checked\"> Remember me\n" +
                    "  </div></form>");
            writer.close();
        }
    }

    public void destroy() {

    }
}
