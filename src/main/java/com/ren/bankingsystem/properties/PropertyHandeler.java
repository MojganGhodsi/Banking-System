package com.ren.bankingsystem.properties;

import com.ren.bankingsystem.log.LogHandeler;

import java.io.*;
import java.util.Properties;

/**
 * Created by My PC on 11/3/2017.
 */
public class PropertyHandeler {


        private Properties properties = new Properties();

        public void loadProperties() throws FileNotFoundException {

            try {
                FileReader file = new FileReader("config.properties");
                properties.load(file);

            } catch (IOException e) {
                e.printStackTrace();
                LogHandeler.getInstance().setWarningLog(e.toString());
            }
        }
        //just first time
        public void setProperties() throws IOException {

            OutputStream os = new FileOutputStream("dataInf.properties");

            properties.put("TypeDAO" , "DB");
            properties.put("DBUrl" , "jdbc:derby:demo;create=true");
            properties.put("PBReportPdfPath" , "d:\\PhoneBooksReport.pdf");
            properties.put("PBReportExcelPath" , "d:\\PhoneBooksReport.xlsx");
            properties.put("CReportPdfPath" , "d:\\ContactsReport.pdf");
            properties.put("CReportExcelPath" , "d:\\ContactsReport.xlsx");
            properties.put("PNReportPdfPath" , "d:\\PhoneNumbersReport.pdf");
            properties.put("PNReportExcelPath" , "d:\\PhoneNumbersReport.xlsx");

            properties.store(os , null);

            os.close();

        }

        public String getProperties(String key) {
            return (String) properties.get(key);
        }


    }


