package com.ren.bankingsystem.control.validationcheck;

import com.ren.bankingsystem.view.impl.UI;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by My PC on 11/4/2017.
 */
public class ValidationCheck {

    private static ValidationCheck validationcheck= new ValidationCheck();

    private final Pattern emailPattern =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private final Pattern namePattern =
            Pattern.compile("[a-zA-z]", Pattern.CASE_INSENSITIVE);
    private final Pattern customerNumberPattern =
            Pattern.compile("[0-9]{8}");
    private final Pattern depositNumberPattern =
            Pattern.compile("^[0-9]{1,10}$");
    private final Pattern amountPattern =
            Pattern.compile("^[1-9]([0-9]{1,45}$)");

    private UI ui = UI.getInstance();

    private ValidationCheck() {

    }

    public static ValidationCheck getInstance() {
        return validationcheck;
    }

    public boolean checkValidation(String input, Value type) {
        Matcher matcher;
        switch (type) {
            case EMAIL:
                matcher = emailPattern.matcher(input);
                if (matcher.find())
                    return true;
                else
                System.out.println("invalied email try from first");
                    ui.displayCustomerMenu();
            case NAME:
                matcher = namePattern.matcher(input);
                if (matcher.find())
                    return true;
                if (input.equals(null) || input.equals("") || !matcher.find())
                    System.out.println("invalied name try from first");
                    ui.displayCustomerMenu();

            case DEPOSITNEMBER:
                matcher = depositNumberPattern.matcher(input);
                if (matcher.find())
                    return true;
                else
                    matcher.find();
                    ui.displayDepositMenu();

            case CUSTOMERNUMBER:
                matcher = customerNumberPattern.matcher(input);
                if (matcher.find())

                    return true;
                else
                    ui.displayCustomerMenu();

            case AMOUNT:
                matcher = amountPattern.matcher(input);
                if (matcher.find())
                    return true;
                else
                    ui.displayDepositMenu();

            default:
                ui.exitProgram();
        }

        return true;
    }
}
