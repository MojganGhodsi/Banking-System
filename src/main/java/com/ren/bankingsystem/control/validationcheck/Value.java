package com.ren.bankingsystem.control.validationcheck;

/**
 * Created by My PC on 11/4/2017.
 */
public enum Value {

    EMAIL,
    AMOUNT,
    CUSTOMERNUMBER,
    DEPOSITNEMBER,
    NAME
}
